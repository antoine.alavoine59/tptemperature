#include <Arduino.h>
#include <Wire.h> //pour accéder au bus I2C
#include "SparkFunBME280.h" //inclusion de la lib BMP280

BME280 capteurBMP280; //pour communiquer avec le capteur
int sdaBMP280 = D2; //broche SDA BMP280
int sclBMP280 = D1; //broche SCL BMP280
int addrI2CBMP280 = 0x77; //adresses I2C BMP280
float pression = 0.0; //pour stocker la pression
float temperature = 0.0; //pour stocker la température
String msgConsole = ""; //message pour la console
void setup()
{
Serial.begin(115200);
Wire.begin(sdaBMP280, sclBMP280); //Conf de la liaison I2C
capteurBMP280.setI2CAddress(addrI2CBMP280); //adresse I2C du BMP280
if (capteurBMP280.beginI2C(Wire) == false)
Serial.println("BMP280 : communication impossible");
}
void loop()
{
//Pression atmosphérique
pression = capteurBMP280.readFloatPressure() / 100; // en hPa
msgConsole = "Pression : " + (String)pression + " hPa - ";
//température
temperature = capteurBMP280.readTempC();
msgConsole += "Température : " + (String)temperature + " °C";
Serial.println(msgConsole);
delay(500);
}